function __fish_complete_ogrextentions
	python3 -c "
from osgeo import gdal;
for ext in sorted(list(set(' '.join([drv.GetMetadataItem(gdal.DMD_EXTENSIONS) for drv in [gdal.GetDriver(index) for index in range(gdal.GetDriverCount())] if (drv.GetMetadataItem(gdal.DCAP_VECTOR) and drv.GetMetadataItem(gdal.DMD_EXTENSIONS) != None)]).split(' ')))):
	print(ext)
"
end
#TODO
#Long names as grayout (drv.GetMetadataItem(gdal.DMD_LONGNAME)