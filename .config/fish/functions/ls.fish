function ls --description 'List contents of directory'
set -l opt --group-directories-first --color=auto
isatty stdout
and set -a opt -FX
command ls $opt $argv
end
