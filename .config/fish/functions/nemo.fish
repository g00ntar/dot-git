function nemo --description 'Run nemo from commanline and disown process'
bash -c "nemo $argv > /dev/null 2>&1 & disown \$!"
end
