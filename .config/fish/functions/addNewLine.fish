function addNewLine
git ls-files -z | while IFS= read -d '' f; tail -c1 < $f | read _ || echo >> $f; end
end
