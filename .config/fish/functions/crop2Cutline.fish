# Defined in - @ line 1
function crop2Cutline --description 'gdalwarp -multi -crop_to_cutline -dstalpha -of GTiff -cutline CUTLINE SRC_DS DST_DS'
	gdalwarp -multi --config GDAL_CACHEMAX 4096 -wo "NUM_THREADS=10" -wm 4096 -co compress=DEFLATE -co ZLEVEL=9 -crop_to_cutline -dstalpha -of GTiff -cutline $argv;
end
