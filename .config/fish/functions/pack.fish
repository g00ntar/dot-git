# TODO: ADD 7za
# TODO: ADD zip
# TODO: drop pv dependency with checkpoint feature
# TODO: Add utilisation os USR1 signal

function pack --description "Tar GUI wrapper"
    # set -l SHOW_PROGRESS false
    set -l SHOW_DIALOG false
    set -l OUTPUT_FILE (mktemp pack_output.XXX.tar)

    for option in $argv
        switch "$option"
            # case -p --progress
            #     set -l SHOW_PROGRESS true
            #     set --erase argv[1]
            case -d --dialog
                # set -l SHOW_PROGRESS true
                set SHOW_DIALOG true
                set --erase argv[1]
            case \*.tar\*
                # echo FILE:$option
                set OUTPUT_FILE $option
                set --erase argv[1]
                break
            # case \*
            #     printf "error: Unknown option %s\n" $option
        end
    end
    set -l DATA $argv
    set -l DATA_SIZE (du -scb $DATA 2>/dev/null | grep total | cut -f1)
    set -l CHECKPOINT (math $DATA_SIZE/50)
    set -l FILE_NAME_ARRAY = (string split . $OUTPUT_FILE)
    # set --erase $argv[1..-1]

    # echo OUTPUT_FILE:$OUTPUT_FILE
    echo DATA_SIZE:$DATA_SIZE
    # echo FILE_EXT:$FILE_NAME_ARRAY[-1]
    echo "============="

    switch $FILE_NAME_ARRAY[-1]
        case gz
            _pack_pigz $DATA_SIZE $OUTPUT_FILE $DATA 
        
        case xz
            _pack_xz $DATA_SIZE $OUTPUT_FILE $DATA 
        
        case bz2
            _pack_bpzip2 $DATA_SIZE $OUTPUT_FILE $DATA 
    end

    function _pack_bpzip2
        tar --totals=USR1 --create -f - $argv[3..-1] | pv -s $argv[1] | pbzip2 > $argv[2]
    end

    function _pack_xz
        tar --totals=USR1 --create -f - $argv[3..-1] | pv -s $argv[1] | xz -9 -T0 > $argv[2]
    end

    function _pack_pigz
        tar --totals=USR1 --create -f - $argv[3..-1] | pv -s $argv[1] | pigz -9 > $argv[2]
    end

# tar --totals=USR1 --create -f - $DATA | pv -s $DATA_SIZE

# --record-size=1K
# --checkpoint=$CHECKPOINT\
# --checkpoint-action="ttyout=>"

# | pbzip2 > $OUTPUT_FILE.tar.bz2
# | xz -9 -T0 > $OUTPUT_FILE.tar.xz
# | pigz -9 > $OUTPUT_FILE.tar.gz

end