function qgis --description 'Run QGIS from commanline and disown process'
bash -c "LOG=\$(mktemp /tmp/qgis.XXX.log); qgis $argv > \$LOG 2>&1 & disown \$! && echo \$LOG"
end
