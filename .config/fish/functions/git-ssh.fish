function git-ssh --description 'update ssh config repo'
    	git --git-dir="$HOME/.config/ssh" --work-tree="$HOME/.ssh" $argv
end
