function lab --description 'Run Jupyter Lab'
set TEMP_DIR (mktemp -d $XDG_RUNTIME_DIR/jupyter.XXXX)
set DATA_DIR $PWD
ln -s $DATA_DIR $TEMP_DIR/data; and \
ln -s $HOME/dev/notebooks $TEMP_DIR/; and \
cd $TEMP_DIR; and \
jupyter lab --no-browser --ip 0.0.0.0 $argv
cd $DATA_DIR; and \
rm -rfI $TEMP_DIR
end
