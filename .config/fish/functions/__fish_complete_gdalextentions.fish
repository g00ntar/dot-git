function __fish_complete_gdalextentions
	python3 -c "
from osgeo import gdal;
for ext in sorted(list(set(' '.join([drv.GetMetadataItem(gdal.DMD_EXTENSIONS) for drv in [gdal.GetDriver(index) for index in range(gdal.GetDriverCount())] if (drv.GetMetadataItem(gdal.DCAP_RASTER) and drv.GetMetadataItem(gdal.DMD_EXTENSIONS) != None)]).split(' ')))):
	print(ext)
"
end
    